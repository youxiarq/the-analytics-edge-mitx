#Final exam Q1
setwd("C:\\Users\\ranger.idea\\Downloads\\The edge of analytical")
Airlines<-read.csv("AirlineDelay.csv")
str(Airlines)
summary(Airlines)
set.seed(15071)

spl = sample(nrow(Airlines), 0.7*nrow(Airlines))

AirlinesTrain = Airlines[spl,]

AirlinesTest = Airlines[-spl,]

dim(AirlinesTrain)
dim(AirlinesTest)

#library("caTools")
#?sample.split

#Linear regression model with all variable
model1<-lm(TotalDelay~.,data=AirlinesTrain)
summary(model1)

colnames(AirlinesTrain)
cor(AirlinesTrain[,c(5,6,14,15)])

pred.test<-predict(model1,newdata=AirlinesTest)
str(pred.test)
sse<-sum((AirlinesTest$TotalDelay-pred.test)^2)
sse
sst<-sum((AirlinesTest$TotalDelay-mean(AirlinesTrain$TotalDelay))^2)
sst
1-(sse/sst)

Airlines$DelayClass = factor(ifelse(Airlines$TotalDelay == 0, "No Delay", ifelse(Airlines$TotalDelay >= 30, "Major Delay", "Minor Delay")))
table(Airlines$DelayClass)
Airlines$TotalDelay = NULL

library(caTools)
set.seed(15071)
spl<-sample.split(Airlines$DelayClass,0.7)
AirlinesTrain<-Airlines[spl,]
AirlinesTest<-Airlines[!spl,]
dim(AirlinesTrain)
dim(AirlinesTest)
dim(Airlines)
library("rpart")
model2<-rpart(DelayClass~.,data=AirlinesTrain,method="class")
(model2)
library("rpart.plot")
prp(model2)

pred.model2<-predict(model2,newdata=AirlinesTrain,type="class")
str(pred.model2)
table(pred.model2,AirlinesTrain$DelayClass)
(361+3094)/nrow(AirlinesTrain)

table(AirlinesTrain$DelayClass)
(3282)/nrow(AirlinesTrain)

pred.model2.test<-predict(model2,newdata=AirlinesTest,type="class")
table(pred.model2.test,AirlinesTest$DelayClass)
(153+1301)/nrow(AirlinesTest)

######
#Question 2
######
rm(list=ls())

eBay<-read.csv("ebay.csv",stringsAsFactors=FALSE)
mean(eBay$sold)
summary(eBay)
sort(table(eBay$size))
str(eBay)
eBay$sold<-as.factor(eBay$sold)
eBay$condition<-as.factor(eBay$condition)
eBay$heel<-as.factor(eBay$heel)
eBay$style<-as.factor(eBay$style)
eBay$color<-as.factor(eBay$color)
eBay$material<-as.factor(eBay$material)
str(eBay)

set.seed(144)

library(caTools)

spl = sample.split(eBay$sold, 0.7)
training<-eBay[spl,]
testing<-eBay[!spl,]
dim(training)
dim(testing)

model1<-glm(sold~biddable+startprice+condition+heel+style+color+material,data=training,family=binomial)
summary(model1)

100*(-0.0044423)+(-0.4952981)+(0.1224260)+0+(0.2226547)+(-1.1078098)+0.5990788
exp(-1.103178)/(exp(-1.103178)+1)

pred.model1<-predict(model1,newdata=testing,type='response')
str(pred.model1)
table(pred.model1>=0.5,testing$sold)
table(training$sold)

library(ROCR)
prediction.model1<-prediction(pred.model1,testing$sold)
performance(prediction.model1,'auc')@y.values
perf<-performance(prediction.model1,measure='tpr',x.measure='fpr')
plot(perf, colorize=TRUE,print.cutoffs.at=seq(0,1,by=0.1), text.adj=c(-0.2,1.7))

#tree model
library("rpart")
library(caret)
library(e1071)

expNumFold<-trainControl( method = "cv", number = 10 )
cpGrid = expand.grid( .cp = seq(0.001,0.05,0.001)) 
# Perform the cross validation
results<-train(sold~biddable+startprice+condition+heel+style+color+material, data = training, method = "rpart", trControl = expNumFold, tuneGrid = cpGrid )

resutlts
model2<-rpart(sold~biddable+startprice+condition+heel+style+color+material,data=training,cp=0.006)
model2
prp(model2)

text.prepare<-function(data){
  library(tm)
  CorpusHeadline = Corpus(VectorSource(data))
  CorpusHeadline = tm_map(CorpusHeadline, tolower)
  
  # Remember this extra line is needed after running the tolower step:
  
  CorpusHeadline = tm_map(CorpusHeadline, PlainTextDocument)
  
  CorpusHeadline = tm_map(CorpusHeadline, removePunctuation)
  
  CorpusHeadline = tm_map(CorpusHeadline, removeWords, stopwords("english"))
  
  CorpusHeadline = tm_map(CorpusHeadline, stemDocument)
  
  dtm = DocumentTermMatrix(CorpusHeadline)
  
  return(dtm)
}

dtm<-text.prepare(eBay$description)

dim(dtm)
(dtm)
spdtm<-removeSparseTerms(dtm,0.9)
spdtm
descriptionText<-as.data.frame(as.matrix(spdtm))
sort(colSums(descriptionText))

names(descriptionText) = paste0("D", names(descriptionText))
colnames(eBay)

descriptionText$sold<-eBay$sold
descriptionText$biddable<-eBay$biddable
descriptionText$startprice<-eBay$startprice
descriptionText$condition<-eBay$condition
#descriptionText$size<-eBay$size
descriptionText$heel<-eBay$heel
descriptionText$style<-eBay$style
descriptionText$color<-eBay$color
descriptionText$material<-eBay$material

dim(descriptionText)

trainText<-descriptionText[spl,]
testText<-descriptionText[!spl,]
dim(testText)

glmText<-glm(sold~.,data=trainText,family="binomial")
summary(glmText)

computeAUC<-function(pred.model1,sold){
  library(ROCR)
  prediction.model1<-prediction(pred.model1,sold)
  print(performance(prediction.model1,'auc')@y.values)
  perf<-performance(prediction.model1,measure='tpr',x.measure='fpr')
  plot(perf, colorize=TRUE,print.cutoffs.at=seq(0,1,by=0.1), text.adj=c(-0.2,1.7))
}

computeAUC(predict(glmText,type="response"),trainText$sold)
computeAUC(predict(glmText,newdata=testText,type="response"),testText$sold)

##################
#Question 3
#################
rm(list=ls())
hubway<-read.csv("HubwayTrips.csv")
dim(hubway)
mean(hubway$Duration)
mean(subset(hubway,Weekday==1)$Duration)
mean(subset(hubway,Weekday==0)$Duration)
tapply(hubway$Duration,hubway$Weekday,mean)
colSums(hubway)
summary(hubway)

library(caret)
preproc<-preProcess(hubway)
hubwaynorm = predict(preproc, hubway)

summary(hubwaynorm)
#summary(scale(hubway))
#?dist
set.seed(5000)
#?kmeans
result1<-kmeans(hubwaynorm,10)
str(result1)
sort(result1$size)

result1$centers

set.seed(8000)
result2<-kmeans(hubwaynorm,20)
sort(result2$size)
result2$centers
tmp.mean<-apply(result2$centers,2,mean)
subset(result2$centers,(result2$centers[,4]>=0)&(result2$centers[,5]>=0))
tmp.mean

boxplot(hubway$Age~result1$cluster)
library(ggplot2)
tmp.plot<-as.data.frame(cbind(hubway$Age,result1$cluster))
colnames(tmp.plot)<-c("age","cluster")
head(tmp.plot)
ggplot(tmp.plot,aes(x=age,y=cluster))+geom_point()

#ggplot(tmp.plot,aes(x=cluster,y=age))+geom_histogram()
